/*
 * This file is part of COMP332 Assignment 1.
 *
 * Copyright (C) 2019 Dominic Verity, Macquarie University.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.mq.frogsandtoads

import java.util
import doodle.image.Image
import scala.collection.JavaConverters._
import doodle.image._
import doodle.image.syntax._
import doodle.core._

/**
  * A puzzle state is given as a 1-dimensional array of cell values.
  */
class PuzzleState private (board: Vector[PuzzleState.Cell], loc: Int) {

  import PuzzleState._

  val size = board.size
  val emptyLoc = loc

  def isTerminalState(): Boolean = {
    board.slice(0, emptyLoc).forall(_ == Toad) &&
      board(emptyLoc) == Empty &&
      board.slice(emptyLoc + 1, size).forall(_ == Frog)
  }

  def isInitialState(): Boolean = {
    board.slice(0, emptyLoc).forall(_ == Frog) &&
      board(emptyLoc) == Empty &&
      board.slice(emptyLoc + 1, size).forall(_ == Toad)
  }

  // FIXME you might want to add more methods here.

  def getBoard() : Vector[PuzzleState.Cell] = {
    return board;
  }

  def getLoc() : Int = {
    return loc;
  }

}

/**
  * Companion object for the [[PuzzleState]] class, provides a public constructor.
  */
object PuzzleState {

  /**
    * Case class for case objects to represent the possible contents of a
    * cell in the puzzle.
    */
  sealed abstract class Cell
  case object Frog extends Cell
  case object Toad extends Cell
  case object Empty extends Cell


  /**
    * Construct a [[PuzzleState]] object in the initial state for a
    * puzzle with specified numbers of frogs and toads.
    *
    * @param frogs number of frogs to place on the left of the [[PuzzleState]]
    * to be constructed
    * @param toads number of toads top place on the right of the [[PuzzleState]]
    * to be constructed
    */
  def apply(frogs: Int, toads: Int): PuzzleState = {
    if (frogs <= 0 || frogs > 10)
      throw new Exception("The number of frogs must be between 1 and 10.")

    if (toads <= 0 || toads > 10)
      throw new Exception("The number of frogs must be between 1 and 10.")

    new PuzzleState(
      Vector.fill(frogs)(Frog) ++ Vector(Empty) ++
        Vector.fill(toads)(Toad),
      frogs
    )
  }

  /**
    * Find a sequence of legal moves of the frogs and toads puzzle from a specified starting
    * [[PuzzleState]] to the terminal [[PuzzleState]].
    *
    * @param start the starting [[PuzzleState]]
    * @return the sequence of [[PuzzleState]] objects passed through in the transit from
    * state `start` to the terminal state (inclusive). Returns the empty sequence if no solution
    * is found.
    */
  def solve(start: PuzzleState): Seq[PuzzleState] = {
    // FIXME add your frogs and toads solver code here.
    var result : List[PuzzleState] = List();
    //定义深度优先搜索的map
    var resultMap : util.Map[String, Object] = new util.HashMap[String, Object]();
    resultMap.put("status", start);//当前状态
    resultMap.put("nexts", new util.ArrayList[util.Map[String, Object]]());//下一状态的集合
    find(resultMap, start.getBoard().apply(0), start.getBoard().apply(start.getBoard().length - 1));
    //深度优先搜索后，遍历结果集，查找出所有链路
    var resultList: util.List[util.List[PuzzleState]] = new util.ArrayList[util.List[PuzzleState]]();
    var pathstack: util.Stack[util.Map[String, Object]] = new util.Stack[util.Map[String, Object]]();
    iteratorNode(resultMap, pathstack, resultList);
    //查找完成
    if(resultList != null && resultList.size() > 0){
      var puzzleStateList: util.List[PuzzleState] = resultList.get(0);
      for(i <- 0 to puzzleStateList.size() - 1){
        result = puzzleStateList.get(i) +: result;
      }
      result = result.reverse;
    }
    return result;
    //Seq()
  }

  /**
    * Call [[solve]] to generate a sequence of legal moves from a specified
    * starting [[PuzzleState]] to the terminal [[PuzzleState]]. Render each state in that solution as
    * an image and return the resulting sequence of images.
    *
    * @param start the starting [[PuzzleState]]
    * @return the sequence of [[Image]] objects depicting the sequence of puzzle states
    * passed through in the transit from the `start` state to the terminal state.
    */
  def animate(start: PuzzleState): Seq[Image] = {
    // FIXME add your code here to generate the animation frame sequence.
    var puzzleStateList: Seq[PuzzleState] = solve(start);
    val frogSquare = Image.square(100).fillColor(Color.green);
    val toadsSquare = Image.square(100).fillColor(Color.blue);
    val spaceSquare = Image.square(100).fillColor(Color.white);
    val puzzleStateListLength = puzzleStateList.length;
    var result: List[Image] = List();
    for(i <- 0 to puzzleStateListLength - 1){
      val puzzleState: PuzzleState = puzzleStateList.apply(i);
      val vector: Vector[PuzzleState.Cell] = puzzleState.getBoard();
      val vectorLength: Int = vector.length;
      var tempImage: Image = null;
      for(j <- 0 to vectorLength - 1){
        val currentCell: Cell = vector.apply(j);
        if(Frog.equals(currentCell)){
          if(tempImage == null){
            tempImage = frogSquare;
          }else{
            tempImage = tempImage.beside(frogSquare);
          }
        }else if(Toad.equals(currentCell)){
          if(tempImage == null){
            tempImage = toadsSquare;
          }else{
            tempImage = tempImage.beside(toadsSquare);
          }
        }else{
          if(tempImage == null){
            tempImage = spaceSquare;
          }else{
            tempImage = tempImage.beside(spaceSquare);
          }
        }
      }
      result = tempImage +: result;
    }
    return result.reverse;
    //Seq();
  }

  /**
    * Create an animation of a solution to the frogs and toads puzzle, starting from the initial
    * [[PuzzleState]] and ending at the terminal [[PuzzleState]].
    *
    * @param frogs the number of frogs in the puzzle (between 1 and 10 inclusive)
    * @param toads the number of toads in the puzzle (between 1 and 10 inclusive)
    */
  def animate(frogs: Int, toads: Int): Seq[Image] =
    animate(PuzzleState(frogs, toads))

  //FIXME You might want to add some (private) auxiliary functions here.

  def find(resultMap: util.Map[String, Object], leftKind: Cell, rightKind: Cell): Unit = {
    var currentStatus: PuzzleState = resultMap.get("status").asInstanceOf[PuzzleState];
    var nexts: util.List[util.Map[String, Object]] = resultMap.get("nexts").asInstanceOf[util.List[util.Map[String, Object]]];
    //列出所有可能变更的下一状态
    var current: Vector[PuzzleState.Cell] = currentStatus.getBoard();
    //先从前往后遍历看看有什么可以动的，再从后往前遍历看看有什么可以动的
    var nextStatusList: util.List[PuzzleState] = new util.ArrayList[PuzzleState]();
    for(i <- 0 to current.length - 1){
      //从左向右移动
      if(i < current.length - 1 && current.apply(i).equals(leftKind)){//只能是左边的类型向右移动
      var str: Cell = current.apply(i);
        var nextStr: Cell = current.apply(i+1);
        if(Empty.equals(nextStr)){//如果下一个是空位置，那就可以向右平移
        var next: Vector[PuzzleState.Cell] = swarp(current, i, i+1);//交换位置，相当于移动
        var newStatus: PuzzleState = new PuzzleState(next, currentStatus.getLoc() + 1);
          nextStatusList.add(newStatus);
        }else if(i < current.length - 2 && !str.equals(nextStr) && Empty.equals(current.apply(i+2))){
          //右边是个异类，且异类后面是个空白,可以跳过去
          var next: Vector[PuzzleState.Cell] = swarp(current, i, i+2);//交换位置，相当于移动
          var newStatus: PuzzleState = new PuzzleState(next, currentStatus.getLoc() + 1);
          nextStatusList.add(newStatus);
        }
      }
    }
    for(i <- 0 to current.length - 1){
      var j = (current.length - 1) - i;
      if(j > 0 && current.apply(j).equals(rightKind)){
        val str : Cell = current.apply(j);
        val nextStr : Cell = current.apply(j-1);
        if(nextStr.equals(Empty)){
          val next : Vector[PuzzleState.Cell] = swarp(current, j, j-1);
          val newStatus: PuzzleState = new PuzzleState(next, currentStatus.getLoc() + 1);
          nextStatusList.add(newStatus);
        }else if(j > 1 && !str.equals(nextStr) && Empty.equals(current.apply(j-2))){
          val next : Vector[PuzzleState.Cell] = swarp(current, j, j-2);
          val newStatus: PuzzleState = new PuzzleState(next, currentStatus.getLoc() + 1);
          nextStatusList.add(newStatus);
        }
      }
    }
    //计算出所有可能情况后，将结果集放入“nexts”结果中
    for(i <- 0 to nextStatusList.size() - 1){
      var statusMap: util.Map[String, Object] = new util.HashMap[String, Object]();
      statusMap.put("status", nextStatusList.get(i));
      statusMap.put("nexts", new util.ArrayList[util.Map[String, Object]]());
      nexts.add(statusMap);
    }
    resultMap.put("nexts", nexts);
    //遍历下一个状态的结果集，进行递归
    for(i <- 0 to nexts.size() - 1){
      find(nexts.get(i), leftKind, rightKind);
    }
  }

  def iteratorNode(n: util.Map[String, Object], pathstack: util.Stack[util.Map[String, Object]], resultList: util.List[util.List[PuzzleState]]): Unit ={
    pathstack.push(n);//入栈
    var childlist: util.List[util.Map[String, Object]] = n.get("nexts").asInstanceOf[util.List[util.Map[String, Object]]];
    if(childlist == null || childlist.size() == 0){//没有孩子 说明是叶子结点
    var lst: util.List[util.Map[String, Object]] = new util.ArrayList[util.Map[String, Object]]();
      var stackIt: util.Iterator[util.Map[String, Object]]  = pathstack.iterator();
      while(stackIt.hasNext()){
        lst.add(stackIt.next());
      }
      print(lst, resultList);//记录路径，判断是否满足条件
    }else{
      var it: util.Iterator[util.Map[String, Object]] = childlist.iterator();
      while(it.hasNext()){
        var child: util.Map[String, Object] = it.next();
        iteratorNode(child, pathstack, resultList);//深度优先 进入递归
        pathstack.pop();//回溯时候出栈
      }
    }
  }

  def print(lst: util.List[util.Map[String, Object]], resultList: util.List[util.List[PuzzleState]]): Unit ={
    var length: Int = lst.size();
    var pathList: util.List[PuzzleState] = new util.ArrayList[PuzzleState]();
    for(i <- 0 to length - 1){
      var obj: util.Map[String, Object] = lst.get(i);
      pathList.add(obj.get("status").asInstanceOf[PuzzleState]);
      if(i == length - 1){
        //定义反向数组
        var reversion: Vector[PuzzleState.Cell] = lst.get(0).get("status").asInstanceOf[PuzzleState].getBoard().reverse;
        //判断最后一个状态是否和反向数组相同
        if(obj.get("status").asInstanceOf[PuzzleState].getBoard().equals(reversion)){
          resultList.add(pathList);
        }
      }
    }
  }

  def swarp(t: Vector[PuzzleState.Cell], i: Int, j: Int): Vector[PuzzleState.Cell] = {
    var firstTemp: Cell = t.apply(i);
    var secondTemp: Cell = t.apply(j);
    var result: Vector[PuzzleState.Cell] = t.updated(i, secondTemp).updated(j, firstTemp);
    return result;
  }


}
